from again import db 


'''
class Tour(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	tourname = db.Column(db.String(64), index = True, unique = True)
	description = db.Column(db.String(10000))
	images = db.Column(db.String(10))
	price = db.Column(db.Float())
	
	def __init__(self, tourname, description, images, price):
		self.tourname = tourname
		self.description = description
		self.images = images
		self.price = price
	
	def __repr__(self):
		return 'Tour {} '.format(self.tourname)
'''

class turism_for_shamil(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	name = db.Column(db.Text, index = True, unique = True)
	description = db.Column(db.Text)
	images_array = db.Column(db.Text)
	price = db.Column(db.Float())
	difficulty = db.Column(db.Integer)
	categories = db.Column(db.Text)
	popularity = db.Column(db.Integer)
	age = db.Column(db.Integer)
	group_min = db.Column(db.Integer)
	duration = db.Column(db.Float)
	def __init__(self, name, description, images_array, price, difficulty, categories, popularity, age, group_min, duration ):
		self.name = name
		self.description = description
		self.images_array = images_array
		self.price = price
		self.difficulty = difficulty
		self.categories = categories
		self.popularity = popularity
		self.age = age
		self.group_min = group_min
		self.duration = duration
	def __repr__(self):
		tour_dict = {"id" : self.id, "images" : self.images_array, "price" : self.price, "tur_desc" : self.description, 
		"tur_name" : self.name, "difficulty" : self.difficulty, "categories" : self.categories, "popularity" : self.popularity,
		"age" : self.age, "group_min" : self.group_min, "duration" : self.duration}
		return str(tour_dict)
		
class tur_comments(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	user_id = db.Column(db.Integer)
	tur_id = db.Column(db.Integer)
	body = db.Column(db.Text)
	creation_time = db.Column(db.BigInteger)
	change_time = db.Column(db.BigInteger)
	
	def __init__(self, user_id, tur_id, body, creation_time, change_time):
		self.user_id = user_id
		self.tur_id = tur_id
		self.body = body
		self.creation_time = creation_time
		self.change_time = change_time
	
	def __repr__(self):
		comments_dict = {"id" : self.id, "user_id" : self.user_id, "tur_id" : self.tur_id, "body" : self.body, 
		"creation_time" : self.creation_time, 
		"change_time" : self.change_time}
		return str(comments_dict)
		
class turism_users(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	email = db.Column(db.Text, index = True, unique = True)
	password = db.Column(db.Text)
	sex = db.Column(db.VARCHAR)
	pasport_number = db.Column(db.Integer)
	birthday_date = db.Column(db.BigInteger)
	creation_date = db.Column(db.BigInteger)
	FIO = db.Column(db.Text)
	
	def __init__(self, email, password, sex, pasport_number, birthday_date, creation_date, FIO):
		self.email = email
		self.password = password
		self.sex = sex
		self.pasport_number = pasport_number
		self.birthday_date = birthday_date
		self.creation_date = creation_date
		self.FIO = FIO
	
	def __repr__(self):
		user_dict = {"id" : self.id, 
					"email" : self.email, 
					"password" : self.password, 
					"sex" : self.sex, 
					"pasport_number" : self.pasport_number, 
					"birthday_date" : self.birthday_date, 
					"creation_date" : self.creation_date, 
					"FIO" : self.FIO}
		return str(user_dict)


class tur_categories(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	category_desc = db.Column(db.Text)
	category_icon = db.Column(db.Text)
	
	def __init__(self, category_desc, category_icon):
		self.category_desc = category_desc
		self.category_icon = category_icon
	
	def __repr__(self):
		categories_dict = {"id" : self.id, "category_desc" : self.category_desc, "category_icon": self.category_icon}
		return str(categories_dict)
		
class tur_date(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	tur_id = db.Column(db.Integer)
	start_date = db.Column(db.BigInteger)
	finish_date = db.Column(db.BigInteger)
	
	def __init__(self, tur_id, start_date, finish_date):
		self.tur_id = tur_id
		self.start_date = start_date
		self.finish_date = finish_date
	
	def __repr__(self):
		date_dict = {"id" : self.id, "tur_id" : self.tur_id, "start_date" : self.start_date, "finish_date" : self.finish_date}
		return str(date_dict)