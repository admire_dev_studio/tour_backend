#!/home/c/cp36696/myenv/bin/python3
from again import again
import os
import sys
import json
import requests
import base64
import time
import random
from datetime import datetime
from flask import Flask, request, jsonify
from flask_cors import CORS
from flaskext.mysql import MySQL
from flask import render_template, flash, redirect, make_response, url_for
from flask_mail import Mail, Message
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from again.forms import TourAddForm
from werkzeug.utils import secure_filename
from again import db
from again.models import turism_for_shamil, turism_users, tur_comments, tur_categories, tur_date
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename

CORS(again)
UPLOAD_FOLDER = r'home/c/cp36696/flask-again/public_html/again/image_turs/'
ICON_FOLDER = r'home/c/cp36696/flask-again/public_html/again/icons/'
application = again



'''
mysql = MySQL()
again.config['MYSQL_DATABASE_USER'] = ''
again.config['MYSQL_DATABASE_PASSWORD'] = ''
again.config['MYSQL_DATABASE_DB'] = ''
again.config['MYSQL_DATABASE_HOST'] = 'localhost'
again.config['UPLOAD_FOLDER'] = "/home/c/cp36696/flask-again/public_html/again/images"
mysql.init_app(again)

conn = mysql.connect()
cursor = conn.cursor()
'''
again.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://cp36696_admireso:social.admire@localhost/cp36696_admireso'
again.config['SECRET_KEY'] = 'you never never never now this a really really really really long secret key'



again.config['MAIL_SERVER'] = 'smtp.googlemail.com'
again.config['MAIL_PORT'] = 587
again.config['MAIL_USE_TLS'] = True
again.config['MAIL_USERNAME'] = ''
again.config['MAIL_DEFAULT_SENDER'] = ''
again.config['MAIL_PASSWORD'] = ''
again.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

	

mail = Mail(again)

categorylsit = ['a'] * 10
@again.route('/writecat', methods = ['GET', 'POST'])
def writecat():
	for categ in categorylsit:
		db.session.add(tur_categories(categ, "icon.jpg"))
		db.session.commit()
		db.session.close()
	return jsonify({"result" : "good"})

cat_list = list(range(1, 35))
price_list = [100000, 200000, 300000, 400000, 500000, 1000000, 15000000]
dif_list = [1, 2, 3, 4]
pop_list = list(range(1, 25))
age_list = [6, 8, 12, 14, 16, 18]
grup_list = [1, 2, 5, 10, 16, 20, 25]
dur_list = list(range(1, 10))
samplist = [1, 2, 3, 4, 5]


@again.route('/rewrite', methods = ['GET', 'POST'])
def rewrite():
	for i in range(1, 51):
		tour = turism_for_shamil.query.get(i)
		tour.images_array = json.dumps(["Screenshot_13.png"])
		db.session.commit()
		db.session.close()
	return jsonify({"result" : "good"})


@again.route('/write', methods = ['GET', 'POST'])
def writ():
	cat_list = list(range(1,18))
	tour_list = eval(str(turism_for_shamil.query.all()))
	for tur in tour_list:
		tour = turism_for_shamil.query.get(tur["id"]) 
		if tour.price == 15000000:
			tour.price = 1500000
		db.session.commit()
		db.session.close()
	return jsonify({"result" : "good"})




@again.route('/writecat', methods = ['GET', 'POST'])
def write():
	for i in range(1,12):
		category = tur_categories.query.get(i)
		category.category_icon = "favicon.ico"
		db.session.commit()
		db.session.close()
	return jsonify({"result" : "good"})

@again.route('/createtable', methods = ['GET', 'POST'])
def createtable():
	db.create_all()
	return make_response("good", 200)
	
@again.route('/api/mail', methods = ['GET', 'POST'])
def pochta():
	data = request.data
	json = eval(data)
	body = str(json["body"])
	msg = Message("Вопрос с сайт TourProject", recipients = ['koptseff.mikhail@yandex.ru'])
	msg.body = body
	mail.send(msg)
	#return make_response("good", 200)
	return jsonify({"result" : "good"})


@again.route('/api/db/increase_pop/<ID>')
def increace(ID):
	ID = int(ID)
	tour = turism_for_shamil.query.get(ID)
	tour.popularity += 1 
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})

@again.route('/api/db/update_tur', methods = ['GET', 'POST'])
def upd_tur():
	files = request.files.to_dict()
	file_list = []
	for file in files:
		file_name = str(time.time()) + str(random.randint(0, 999)) + ".png"
		path_wr = UPLOAD_FOLDER + file_name
		file_list.append(file_name)
		files[file].save(path_wr)
	ID = str(request.form.get("id", ""))
	tur = str(request.form.get("tur_name", ""))
	description = str(request.form.get("tur_desc", ""))
	deleted_photos = eval(str(request.form.get("deleted_photos", "[]")))
	difficulty = str(request.form.get("difficulty", ""))
	categories = str(request.form.get("categories", "[]"))
	price = str(request.form.get("price", ""))
	tour = turism_for_shamil.query.get(ID)
	if tur:
		tour.name = tur
	if description:
		tour.description = description
	if deleted_photos:
		img = tour.images_array
		img = eval(img)
		for phot in deleted_photos:
			img.remove(phot)
			os.remove(UPLOAD_FOLDER + phot)
		tour.images_array = json.dumps(img)
	if file_list:
		img = eval(tour.images_array)
		tour.images_array = json.dumps(img + file_list)
	if price:
		tour.price = price
	if difficulty:
		tour.difficulty = difficulty
	if categories:
		tour.categoris = json.dumps(categories)
	if popularity:
		tour.opularity = popularity
	if popularity:
		tour.opularity = popularity
	if age:
		tour.age = age
	if group_min:
		tour.group_min = group_min
	if duration:
		tour.duration = duration
	db.session.commit()
	db.session.close()
	#return make_response("good", 200)
	return jsonify({"result" : "good"})
	
@again.route('/api/db/add_tur', methods = ['GET', 'POST'])
def add():
	files = request.files.to_dict()
	file_list = []
	for file in files:
		file_name = str(time.time()) + str(random.randint(0, 999)) + ".png"
		path_wr = UPLOAD_FOLDER + file_name
		file_list.append(file_name)
		files[file].save(path_wr)
	#path_list =[]
	tur = str(request.form.get("tur_name", ""))
	description = str(request.form.get("tur_desc", ""))
	difficulty = str(request.form.get("difficulty", ""))
	categories = str(request.form.get("categories", "[]"))
	price = str(request.form.get("price", ""))
	popularity = str(request.form.get("popularity", ""))
	age = str(request.form.get("age", ""))
	group_min = str(request.form.get("group_min", ""))
	duration = str(request.form.get("duration", ""))
	db.session.add(turism_for_shamil(tur, description, json.dumps(file_list), price, difficulty, json.dumps(categories), popularity, age, group_min, duration))
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})

@again.route('/query', methods = ['GET', 'POST'])
def query():
	return jsonify(str(turism_for_shamil.query.all()))

@again.route('/api/db/registration', methods = ['GET', 'POST'])
def registration():
    data = request.data
    json = eval(data)
    email = str(json["email"])
    password = str(json["password"])
    password = generate_password_hash(password)
    creation_date = str(time.time())
    pasport_number = str(json["pasport_number"])
    birthday_date = str(json["birthday_date"])
    sex = str(json["sex"])
    FIO = str(json["fio"]) 
    db.session.add(turism_users(email, password, sex, pasport_number, birthday_date, creation_date, FIO))
    db.session.commit()
    db.session.close()
    return jsonify({"result" : "good"})

@again.route('/api/auth', methods = ['GET', 'POST'])
def autorithation():
	data = request.data
	json = eval(data)
	email = str(json["email"])
	password = str(json["password"])
	user = turism_users.query.filter_by(email=email).first()
	user = eval(str(user))
	if user:
		check = check_password_hash(str(user["password"]), password)
		if check == True:
			return jsonify({"result" : "All right"})
		else:
			return jsonify({"result" : "Bad password"})
	else:
		return jsonify({"result" : "Unknown email"})
	

@again.route('/touradd', methods = ['GET', 'POST'])
def touradd():
	form = TourAddForm(request.form)
	if form.validate_on_submit():
		return "good"
	return render_template('touradd.html', title = "Добавить тур", form=form)

@again.route('/')
def hello_world():
    return render_template('index.html')
    
@again.errorhandler(404)
def not_found_error(error):
	return render_template('index.html')

@again.route('/api/similar_tours/<ID>', methods = ['GET'])
def similars(ID):
	tur_cat = {int(i) for i in set(eval(turism_for_shamil.query.get(int(ID)).categories))}
	def notself(tur1):
		return tur1.id != int(ID)
	def sim(tur):
		return (tur_cat & {int(i) for i in set(eval(tur.categories)) })
	def sort(tur2):
		return len(tur_cat & {int(i) for i in set(eval(tur2.categories)) })
	all_turs = list(filter(notself, turism_for_shamil.query.all()))
	sim_turs = sorted(list(filter(sim, all_turs)), key = sort, reverse = True)
	#sim_json = json.dumps(str(sim_turs))
	#sim_turs = {"sim_turs" : sim_turs}
	#typ = str(type(sim_turs))
	return jsonify(eval(str(sim_turs)))

@again.route('/api/db/view_tur/<ID>', methods=['GET'])
def view(ID):
	tur = eval(str(turism_for_shamil.query.get(ID)))
	now_time = int(time.time())
	tur.update({"dates" : tur_date.query.filter(tur_date.finish_date >= now_time, tur_date.tur_id.in_([ID])).all()})
	return jsonify(eval(str(tur)))
    
@again.route('/api/db/delete_tur/<ID>', methods=['GET'])
def delete(ID):
    db.session.delete(turism_for_shamil.query.filter(id=ID, ).first())
    db.session.commit()
    #return make_response("good", 200)
    return jsonify({"result" : "good"})
	

@again.route('/api/db/list_turs/<page>/<perPage>', methods=['GET'])
def view_page_filter(page, perPage):
	def SortByCost(dic):
		return int(dic['price']) 
	def SortByPop(dic):
		return int(dic['popularity'])
	def SortByDateSmall(dic):
		return int(dic['start_date'])
	def SortByDateLarge(dic):
		return int(dic['dates'][0]['start_date'])
	page = int(page)
	perPage = int(perPage)
	tur_list = []
	now_time = int(time.time())
	filt = request.args.get('filter', default = "expensive", type = str)
	min_date = str(request.args.get('min_date', default = 0, type = int))
	max_date = str(request.args.get('max_date', default = 10000000000000000, type = int))
	min_price = float(request.args.get('min_price', default = 0, type = str))
	max_price = float(request.args.get('max_price', default = 10000000000000000, type = str))
	min_duration = float(request.args.get('min_duration', default = 0, type = int))
	max_duration = float(request.args.get('max_duration', default = 10000000000000000, type = int))
	difficulty = request.args.get('difficulty', default = "1,2,3,4", type = str).split(",")
	min_popular = int(request.args.get('min_pop', default = 0, type = str))
	max_popular = int(request.args.get('max_pop', default = 1000000000000000000, type = str))
	categories = set(request.args.get('categories', default = "0", type = str).split(","))
	categories = {int(i) for i in categories}
	
	for j in difficulty:
		tur_list_def = eval(str(turism_for_shamil.query.filter_by(difficulty=str(j)).all()))
		tur_list_def = [tur for tur in tur_list_def if tur['price'] >= min_price and tur['price'] <= max_price  and tur['popularity'] >= min_popular and tur['popularity'] <= max_popular and tur['duration'] >= min_duration and tur['duration'] <= max_duration and ( set(eval(tur['categories'])) & categories or categories == {0})] 
		tur_list = tur_list + tur_list_def
	for j in range(len(tur_list)):
		tur_list[j] = eval(str(tur_list[j]))
		if min_date.find(";") != -1:
			min_date = min_date.split(";")
			max_date = max_date.split(";")
			for k in range(len(min_price)):
				date_list = eval(str(tur_date.query.filter(tur_date.start_date >= min_date[k], tur_date.start_date <= max_date[k], tur_date.tur_id.in_([tur_list[j]["id"]])).all()))
				date_list = sorted(date_list, key = SortByDateSmall)
				date_list = {"dates" : date_list}
		else:
			date_list = eval(str(tur_date.query.filter(tur_date.start_date >= min_date, tur_date.start_date <= max_date, tur_date.tur_id.in_([tur_list[j]["id"]])).all()))
			date_list = sorted(date_list, key = SortByDateSmall)
			date_list = {"dates" : date_list}
		tur_list[j].update(date_list)
	tur_list = [tur for tur in tur_list if tur["dates"] != []]
	if filt == "cheap":
		tur_list = sorted(tur_list, key=SortByCost)
	elif filt == "min_pop":
		tur_list = sorted(tur_list, key=SortByPop)
	elif filt == "most_pop":
		tur_list = sorted(tur_list, key=SortByPop, reverse = True)
	elif filt == "data_normal":
		tur_list = sorted(tur_list, key = SortByDateLarge)
	elif filt == "data_reverse":
		tur_list = sorted(tur_list, key = SortByDateLarge, reverse = True)
	else:
		tur_list = sorted(tur_list, key=SortByCost, reverse = True)	
	page_list = tur_list[(page-1) * perPage: page*perPage - 1 ]
	return jsonify(page_list)


@again.route('/api/db/tour_info')
def tour_info():
	def SortByCost(dic):
		return int(dic['price'])
	def SortByDur(dic):
		return int(dic['duration']) 
	min_price = sorted(eval(str(turism_for_shamil.query.all())), key = SortByCost)[0]['price']
	max_price = sorted(eval(str(turism_for_shamil.query.all())), key = SortByCost, reverse = True)[0]['price']
	min_duration = sorted(eval(str(turism_for_shamil.query.all())), key = SortByDur)[0]['duration']
	max_duration = sorted(eval(str(turism_for_shamil.query.all())), key = SortByDur, reverse = True)[0]['duration']
	categories = eval(str(tur_categories.query.all()))
	
	return jsonify({"min_price" : min_price, "max_price" : max_price, "categories" : categories,"min_duration" : min_duration, "max_duration" : max_duration})
	

@again.route('/api/photo_del', methods = ['POST'])
def photo():
	data = request.data
	json = eval(data)
	ID = str(json["id"])
	images = json['images']
	tour = turism_for_shamil.query.get(ID)
	img_list = eval(tour.images_array)
	tour.images_array =  list(set(img_list) - set(images))
	db.session.commit()
	db.session.close()
	#return make_response("good", 200)
	return jsonify({"result" : "good"})
	
	
@again.route('/api/db/add_comment', methods = ['GET', 'POST'])
def add_comment():
	data = request.data
	path_list =[]
	json = eval(data)
	user_id = str(json["user_id"])
	tur_id = str(json["tur_id"])
	body = str(json["body"])
	creation_time = str(time.time())
	change_time= str(time.time())
	db.session.add(tur_comments(user_id, tur_id, body, creation_time, change_time))
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})
	
@again.route('/api/db/change_comment', methods = ['GET', 'POST'])
def change_comment():
	data = request.data
	json = eval(data)
	ID = str(json["id"])
	body = str(json["body"])
	change_time = str(time.time())
	coment = tur_comments.query.get(ID)
	if body:
		coment.body = body
	coment.change_time = change_time
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})
	
@again.route('/api/db/delete_comment/<ID>', methods=['GET'])
def delete_coment(ID):
    db.session.delete(tur_comments.query.filter_by(id=ID).first())
    db.session.commit()
    db.session.close()
    return jsonify({"result" : "good"})
    
@again.route('/api/db/view_comment/<ID>', methods=['GET'])
def view_comment(ID):
	if ID == 0:
		return jsonify(eval(str(tur_comments.query.all())))
	else:
		return jsonify(eval(str(tur_comments.query.get(ID))))
    
@again.route('/api/db/view_comment_by_tur_id/<ID>', methods=['GET'])
def view_comment_by_id(ID):
	mode = request.args.get('mode', default = "all", type = str)
	comments = []
	if mode == "count":
		count = request.args.get('count', default = 5, type = int)
		for i in range(count):
			comments.append(eval(str(tur_comments.query.filter_by(tur_id = ID)[-1*(i+1)])))
		return jsonify(comments)
	else:
		return jsonify(eval(str(tur_comments.query.filter_by(tur_id = ID).all())))	
    	
 
@again.route('/api/db/add_category', methods = ['GET', 'POST'])
def add_category():
	data = request.files
	#json = eval(data)
	file = data["icon"]
	filename = "icon_" + str(time.time()) + ".png"
	path = ICON_FOLDER + filename
	file.save(path)
	category_name = str(request.form.get('category_name', ''))
	db.session.add(tur_categories(category_name,  filename))
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})

@again.route('/api/db/view_categories', methods=['GET', 'POST'])
def view_categories():
    return jsonify(eval(str(tur_categories.query.all())))

@again.route('/api/db/delete_category/<ID>', methods=['GET'])
def delete_category(ID):
    db.session.delete(tur_categories.query.filter_by(id=ID).first())
    db.session.commit()
    db.session.close()
    return jsonify({"result" : "good"})

@again.route('/api/db/add_tur_date', methods = ['GET', 'POST'])
def add_tur_date():
	data = request.data
	json = eval(data)
	tur_id = str(json["tur_id"])
	start_date = json["start_date"]
	finish_date = json["finish_date"]
	start_date = datetime.strptime(start_date, "%d.%m.%Y %H:%M:%S")
	start_date = time.mktime(start_date.timetuple())
	finish_date = datetime.strptime(finish_date, "%d.%m.%Y %H:%M:%S")	
	finish_date = time.mktime(finish_date.timetuple())
	db.session.add(tur_date(tur_id, str(start_date), str(finish_date)))
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})
	
@again.route('/api/db/delete_old_date', methods = ['GET', 'POST'])
def delete_old_date():
	now_time = int(time.time())
	old_dates = tur_date.query.filter(tur_date.finish_date < now_time).all()
	for date in old_dates:
		db.session.delete(date)
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})
	
@again.route('/api/db/delete_date/<ID>', methods = ['GET', 'POST'])
def delete_date(ID):
	db.session.delete(tur_date.query.get(ID))
	db.session.commit()
	db.session.close()
	return jsonify({"result" : "good"})
	
@again.route('/mail/tur_reg', methods = ['GET', 'POST'])
def tur_reg():
	data = request.data
	json = eval(data)
	name = str(json["name"])
	phone_number = str(json["phone_number"])
	email = str(json["email"])
	people_number = str(json["people_number"])
	comment = str(json["comment"])
	msg = Message("Регистрация на тур", recipients = ['koptseff.mikhail@yandex.ru'])
	msg.body = str("\n Имя " + name + "\n Телефон " + phone_number + "\n email " + email + "\n Количество человек " + people_number + "\n Вопросы и комментарии: " + comment)
	mail.send(msg)
	return jsonify({"result" : "good"})
	
if __name__ == '__main__':
    again.run()