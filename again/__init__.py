#!/home/c/cp36696/myenv/bin/python3
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail
again = Flask(__name__)
again.config.from_object(Config)
db = SQLAlchemy(again)
migrate = Migrate(again, db)


mail = Mail(again)
from again import routes, models