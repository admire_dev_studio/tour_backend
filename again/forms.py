from flask_wtf import FlaskForm
from flask_wtf.file import DataRequired, FileRequired
from wtforms import StringField, TextAreaField, BooleanField, SubmitField, FileField, IntegerField
#from wtforms.validators import FileRequired

class TourAddForm(FlaskForm):
	tourname = StringField("Введите название" )
	description = StringField("Введите описание" )
	price = IntegerField("Введите цену" )
	submit = SubmitField("Добавить тур ")
	images = FileField('Добавить изображения') #добавить , validators = [FileRequired()]